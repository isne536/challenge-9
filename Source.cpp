#include <iostream>
#include "list.h"
using namespace std;

void main()
{
	List test;	//value for check.
	bool value;
	test.headPush(1);	//call push head and show 1.
	test.tailPush(2);	//call push tail and show 2.
	test.headPush(3);	//call push head and show 3.
	test.tailPush(4);	//call push tail and show 4.
	test.display();	//check value.
	int del;
	cout << endl;
	cout << "Input value for delete : ";
	cin >> del;	//input value.
	test.deleteNode(del);	//call function delete.
	cout << endl;
	cout << "Value(after delete) ";	
	cout << endl;
	test.display();	//check value that remain.
	cout << endl;
	int v;
	cout << "Input value for check : ";	//check value in the list.
	cin >> v;	//input value.
	value = test.isInList(v);	//call function for check value.
	cout << endl;
	if (value == true)
	{
		cout << v << " is in the list";
	}
	else
	{
		cout << v << " isn't in the list";
	}
	cout << endl;
	system("pause");
}