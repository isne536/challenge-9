#include <iostream>
#include "list.h"
using namespace std;

List::~List() {
	for (Node *p; !isEmpty(); ) {
		p = head->next;
		delete head;
		head = p;
	}
}

void List::headPush(int front)	//function for input value from head.
{
	Node *data = new Node(front);
	if (head == 0)
	{
		data->next = 0;
		tail = data;
		head = data;
	}
	else
	{
		data->next = head;
		head = data;
	}
}

void List::tailPush(int back)	//function for input value from tail.
{
	Node *data = new Node(back);
	if (tail == NULL)
	{
		head = data;
		tail = head;
	}
	else
	{
		tail->next = data;
		tail = data;
	}
}

int List::headPop()	//function for delete head value.
{
	Node *tmp = head;
	if (head == 0 && tail == 0)
	{
		head = tail = 0;
		return 0;
	}
	else
	{
		int ans = tmp->info;
		head = head->next;
		delete tmp;
		return ans;
	}
}

int List::tailPop()	//function for delete tail value.
{
	Node *tmp = tail;
	if (head == 0 && tail == 0)
	{
		head = tail = 0;
		return 0;
	}
	else
	{
		int ans = tmp->info;
		tail = tail->next;
		delete tmp;
		return ans;
	}
}

void List::deleteNode(int del)	//function for delete value that input.
{
	Node *tmp = head;
	Node *temp = head;
	if (del == temp->info)	//input = head value.
	{
		tmp = head;
		head = head->next;
		delete tmp;
	}
	else if (tail->info == del)	//input = tail value.
	{
		while (tmp->next != 0)
		{
			temp = tmp;
			tmp = tmp->next;
		}
		tail = temp;
		temp->next = 0;
		delete tmp;
	}
	else	//input = mid value.
	{
		while (tmp->info != del)
		{
			tmp = head;
			while (tmp->info != del)
			{
				temp = tmp;
				tmp = tmp->next;
			}
		}
		temp->next = tmp->next;
		delete tmp;
	}
}

bool List::isInList(int val)	//function check value in the list.
{
	Node *data = new Node(*head);
	while (data != 0)
	{
		cout << data->info << endl;
		data = data->next;
	}
	return false;
}

void List::display()	//function for display all value.
{
	Node *data = new Node(*head);
	while (data != 0)
	{
		cout << data->info << endl;
		data = data->next;
	}
}